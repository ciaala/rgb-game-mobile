extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (Array) var types = ['Fences','Cabin','Fountain']
export (Scene) var hitpointSceneConstructor = 'res://instances/hitpoint_scene.tscn'
# Called when the node enters the scene tree for the first time.
func _ready():
	var sceneConstructor = load()
	var destructibles = []
	var positions = $'..'.get_used_cells()
	print('positions:', positions.size())
	for position in positions:
		var index = $'..'.get_cellv(position)
		var name = $'..'.tile_set.tile_get_name(index)
		if types.has(name):
			hitpoint = hitpointSceneConstructor.instance()
			hitpoint.name(position.x + '-' + position.y )
			hitpoint.set_position(position)
			add_child(hitpoint)
			destructibles.append(position)
	print('Destuctibles: ', destructibles.size(), '/', positions.size())
	
func makeHitpoint(position):
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
