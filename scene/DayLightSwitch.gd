extends Light2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node('../../DayNightCycle').connect("DayNightCycle", self, "_on_dayNightCycle")
	enabled = false;

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _on_dayNightCycle(phase) :
	print('Phase:', phase)
	if (phase == 'night'):
		enabled = true;
		
