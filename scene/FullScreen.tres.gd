extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _input(event):

	if event is InputEventScreenDrag:
		print(event, event.relative)
		event = event as InputEventScreenDrag
		var ev = InputEventAction.new()
		if abs(event.relative.x) > abs(event.relative.y): 
			if event.relative.x > 0:
				ev.action = 'ui_right'
			else:
				ev.action = 'ui_left'
		else:
			if event.relative.y > 0:
				ev.action = 'ui_down'
			else:
				ev.action = 'ui_up'
		
		# set as move_left, pressed
		# ev.action = "ui_left"
		ev.pressed = true
		# feedback
		Input.parse_input_event(ev)
		
	if event.is_action_pressed('ui_toggleFullscreen'):
		OS.window_fullscreen = !OS.window_fullscreen
