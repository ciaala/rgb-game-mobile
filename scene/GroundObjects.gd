extends TileMap


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var rocks = 100
export var tree = 100
const LIMIT = 100
export var RANDOM = 1234
# Called when the node enters the scene tree for the first time.
func _ready():
	rand_seed(RANDOM)

	
#tile_set.get_tiles_ids()
	var count = 0
	var tileIndex=tile_set.find_tile_by_name('Rocks')
	for x in range(-LIMIT,LIMIT):
		for y in range (-LIMIT,LIMIT):
			if get_cell(x,y) != -1 :
				continue
			if randi() % 100 == 1:
				set_cell(x, y, tileIndex)
				count += 1
	print(count)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

