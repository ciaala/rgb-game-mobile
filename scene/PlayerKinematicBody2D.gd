extends KinematicBody2D

export var MOTION_SPEED = 32
export var moveStep = 32
var RayNode

func _ready():
	RayNode = get_node('RayCast2D')
	
func _physics_process(_delta): 
	var motion = Vector2(0,0)
	if (Input.is_action_pressed("ui_up")):
		motion += Vector2(0, -moveStep);
		RayNode.set_rotation_degrees(180)
	
	if (Input.is_action_pressed("ui_down")):
		motion += Vector2(0, moveStep);
		RayNode.set_rotation_degrees(0)
	
	if (Input.is_action_pressed("ui_left")):
		motion += Vector2(-moveStep,0)
		RayNode.set_rotation_degrees(270)
	if (Input.is_action_pressed("ui_right")):
		motion += Vector2(moveStep,0)
		RayNode.set_rotation_degrees(90)
	move_and_collide(motion)
	
