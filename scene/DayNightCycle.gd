extends Node
const DAY_IN_MILLIS = 24*3600*1000
const HOUR_IN_MILLIS = 3600*1000
signal DayNightCycle
export var groundTileMapName = 'Land'
export var gameDayInMinutes = 5
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var groundTileMap = null
var globalElapsedTimeMS = 6 * HOUR_IN_MILLIS
var timeModifier = 4
var totalRealDeltaMS = 0;
var previousHour = 6
var currentHour = 5
var millinsInVirtualDayMS = gameDayInMinutes*60*1000.0
var currentLightSlot = 2;
var colorModulate = [
	Color.white.darkened(0.85),
	Color.white.darkened(0.85),
	Color.white.darkened(0.60),
	Color.white.darkened(0.35),
	Color.white.darkened(0.10),
	Color.white.darkened(0.35),
	Color.white.darkened(0.60),
	Color.white.darkened(0.85)
	]
var dayCycle = ['night', 'night', 'day', 'day', 'day', 'day', 'day', 'night']
var currentDayNightSlot = currentLightSlot

# Called when the node enters the scene tree for the first time.
func _ready():
	var siblings = get_parent().get_children()
	for sibling in siblings:
		if sibling.name == groundTileMapName:
			groundTileMap=sibling
	if groundTileMap == null:
		printerr('Unable to find the Tile to control day night cycle')
	# TODO should calculate from current hour
	_updateLight(currentLightSlot)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if delta == 0:
		return
	_updateTime(delta)
	
	
func _updateTime(delta):
	delta = delta*1000
	totalRealDeltaMS += delta*1000;

	var deltaTimeMS = (timeModifier * delta)

	# TODO optimize to minimise math error
	var amountVirtualDayElaped = deltaTimeMS / millinsInVirtualDayMS

	var virtualElapsedTimeMS = (DAY_IN_MILLIS * amountVirtualDayElaped) as int
	globalElapsedTimeMS += virtualElapsedTimeMS
	var currentHour = (globalElapsedTimeMS % DAY_IN_MILLIS) / HOUR_IN_MILLIS
	if (currentHour != previousHour):
		print('CurrentHour: ', currentHour)
		print('TotalRealDeltaMS: ', totalRealDeltaMS )
		previousHour = currentHour
		var lightSlot = (previousHour / 3) % 8
		_updateLight(lightSlot)

func _updateLight(lightSlot):
	if (currentLightSlot != lightSlot ) :
		print('Change light slot: ', lightSlot)
		currentLightSlot = lightSlot
		_updateTileMapColor(currentLightSlot)
	if dayCycle[currentDayNightSlot] != dayCycle[lightSlot]:
		currentDayNightSlot = lightSlot
		print('Notify Day / Night change: ', dayCycle[currentDayNightSlot])
		emit_signal("DayNightCycle", dayCycle[currentDayNightSlot])

func _updateTileMapColor(lightSlot) :
	groundTileMap = groundTileMap as TileMap
	groundTileMap.modulate = self.colorModulate[lightSlot]


